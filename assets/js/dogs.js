// Retrieving element from the HTML page.
let addBtn = document.getElementById('add-btn-basic');
let addBtnForm = document.getElementById('add-btn-form');

let container = document.getElementById("card-container");

// Function that create the template for our card.
// The name and breed can be omitted when called as they have default values
function createCard(name = "default-name", breed = "default-breed") {
    let card = `<div class="card">
    <div class="image-container">
    <img class="dog-img" src="./assets/img/home-image.jpg" />
    </div>
    <span class="dog-name">${name}</span>
    <span class="dog-breed">${breed}</span>
    </div>`

    return card;
}

// Function that adds a card with the default values
function addCardString() {
    
    // Calling function with default values. It's the same as createCard("default-name", "default-breed")
    container.innerHTML += createCard();
}

// Function that adds a card using the value in the form elements
function addCardForm() {
    let nameInput = document.getElementById('dog-name');
    let breedInput = document.getElementById('dog-breed');

    let name = nameInput.value;
    let breed = breedInput.value;

    // Add a card only if both fields are filled
    if(name !== "" && breed !== "")
    {       
        container.innerHTML += createCard(name, breed);
        nameInput.value = "";
        breedInput.value = "";
    }
}

// Registering the event to the buttons
addBtn.addEventListener('click', addCardString);
addBtnForm.addEventListener('click', addCardForm);

// Creating some predefined cards.
let dogs = [
    {
        name: "Dog1",
        breed: "Breed1"
    },
    {
        name: "Dog2",
        breed: "Breed2"
    },
    {
        name: "Dog3",
        breed: "Breed3"
    },
    {
        name: "Dog4",
        breed: "Breed4"
    }
]

for(let dog of dogs) {
    container.innerHTML += createCard(dog.name, dog.breed);
}